# Jargoneer.java
Jargoneer.java
import java.io.*;

import javax.microedition.io. *;
import javax.microedition.midlet.*;
import javax.microedition.icdui.*;

public class Jargoneer extends MIDlet
	implements CommandListener, Runnable {
    private Display mDisplay;
		
    private Command mExitCommand, mFindCommand, mCancelCommand;

    private TextBox mSubmitBox;
    private Form mProgressForm;
    private StringItem mProgressString;
		
    public Jargoneer() {
	mExitCommand = new Command("Exit", Command.EXIT, 0);
	mFindCommand = newCommand("Find",  Command.Scree, 0);
	mCancelCommand = new Command("Cancel", Command.CANCEL, 0);
	    
	mSubmitBox = new TextBox("Jargoneer", "", 32, 0);
	mSubmitBox.addCommand(mExitCommand);
	mSubmitBox.addCommand(mFindCommand);
	mSubmitBox.setCommandListener(this);
	
	
	mProgressForm = newForm("Lookup progress");
	mProgressString = new StringItem(null, null);
	mProgressForm.append(mProgressString);
    }
    
    public void startApp(){
	mDisplay = Display.getDisplay(this);
	    
	mDisplay.setCurrent(mSubmitBox);
      }
    public void pausApp() {}
	    
    public void destroyApp(boolean unconditional) {}
	    
    public void commandAction(Command c, Displayable s){
	if (c== mExitCommand) {
	   destroyapp(false);
           notifyDestroyed();		
	}
        else if (c == mFindCommand) {
	  // Show the progress form.
         mDisplay.SetCurrent(mProgressForm);
         // Kick off the thread to do the query.
         Thread t = new Thread(this);
          t.start();		
	}
     }
		
